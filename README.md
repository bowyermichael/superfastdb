# Super Fast PHP MySQL Database Class
**This is taken from [David Adams at CodeShack](https://codeshack.io/super-fast-php-mysql-database-class/) and created as a repo on GitHub for easy reference and download. All credit goes to CodeShack.**

This lightweight database class has been written with PHP using the MySQLi extension. It uses prepared statements to properly secure your queries, no need to worry about SQL injection attacks.

The MySQLi extension has built-in prepared statements that you can work with that will prevent SQL injection and prevent your database from being exposed, however some developers are confused on how to use these methods correctly so I've created this easy to use database class that'll do the work for you.

This database class is beginner-friendly and easy to implement. With the native MySQLi methods you need to write 3-7 lines of code to retrieve data from a database, however with this class you can do it with just 1-2 lines of code, and is much easier to understand.

## How To Use

### Installing

Add this to your composer.json file
```composer
"repositories": [
    {
        "url": "https://github.com/mchlbowyer/SuperfastDB.git",
        "type": "git"
    }
],
"require": {
    "mchlbowyer/superfast-db": "~0.1-beta"
}
```

### Connect To MySQL Database
```php
use SuperfastDB\SFDatabase;

$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '';
$dbname = 'example';

$sfdb = new SFDatabase($dbhost, $dbuser, $dbpass, $dbname);
```

### Fetch A Record From A Database
```php
$account = $sfdb->query('SELECT * FROM accounts WHERE username = ? AND password = ?', 'test', 'test')->fetchArray();
echo $account['name'];
```
Or you could do:
```php
$account = $sfdb->query('SELECT * FROM accounts WHERE username = ? AND password = ?', array('test', 'test'))->fetchArray();
echo $account['name'];
```

### Fetch multiple records from a database:
```php
$accounts = $sfdb->query('SELECT * FROM accounts')->fetchAll();

foreach ($accounts as $account) {
	echo $account['name'] . '<br>';
}
```
You can specify a callback if you do not want the results being stored in an array (useful for large amounts of data):

```php
$sfdb->query('SELECT * FROM accounts')->fetchAll(function($account) {
    echo $account['name'];
});
```

If you need to break the loop you can add:
```php
return 'break'; 
```

### Get the number of rows:
```php
$accounts = $sfdb->query('SELECT * FROM accounts');
echo $accounts->numRows();
```

### Get the affected number of rows:
```php
$insert = $sfdb->query('INSERT INTO accounts (username,password,email,name) VALUES (?,?,?,?)', 'test', 'test', 'test@gmail.com', 'Test');
echo $insert->affectedRows();
```

### Get the total number of queries:
```php
echo $sfdb->query_count;
```

### Get the last insert ID:
```php
echo $sfdb->lastInsertID();
```

### Close the database:
```php
$sfdb->close();
```

## Conclusion

The database class uses the MySQLi extension, this is built into PHP version >= 5.0.0. If you're using PHP version 5.0.0 to 5.3.0 you'll need install:  [mysqlnd](https://php.net/manual/en/book.mysqlnd.php).

No need to prepare statements using this class, it'll do that for you automatically (write less, do more), your queries will be secure, just remember to make sure you escape your output using  [htmlspecialchars](https://php.net/manual/en/function.htmlspecialchars.php), or your preferred escaping method.

You're free to use this database class in your projects.

Released under the MIT License.